package iterations.binarygap;

// https://codility.com/demo/results/trainingACKT8F-ZJP/
public class Solution {

	public static int solution(int N) {
		String binaryN = Integer.toBinaryString(N);
		int maxGapLength = 0;
		int currentGapLength = 0;
		for (int i = 0; i < binaryN.length(); i++) {
			char zeroOrOne = binaryN.charAt(i);
			if (zeroOrOne == '1') {
				if (currentGapLength > maxGapLength) {
					maxGapLength = currentGapLength;
				}
				currentGapLength = 0;
			} else {
				currentGapLength++;
			}
		}
		return maxGapLength;
	}

	public static void main(String[] args) {
		System.out.println("Biggest gap in 9 is: " + solution(9));
		System.out.println("Biggest gap in 529 is: " + solution(529));
		System.out.println("Biggest gap in 20 is: " + solution(20));
		System.out.println("Biggest gap in 15 is: " + solution(15));
		System.out.println("Biggest gap in 1041 is: " + solution(1041));
	}

}
