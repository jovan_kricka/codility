package countingelements.frogriverone;

import java.util.HashSet;
import java.util.Set;

// https://codility.com/demo/results/trainingGDHQG8-35A/
public class Solution {

	public int solution(int X, int[] A) {
		int latestTime = Integer.MIN_VALUE;
		Set<Integer> leavesPositions = new HashSet<Integer>();
		for (int i = 0; i < A.length; i++) {
			if (A[i] <= X) {
				if (!leavesPositions.contains(A[i]) && i > latestTime) {
					leavesPositions.add(A[i]);
					latestTime = i;
				}
			}
		}
		if (leavesPositions.size() < X) {
			return -1;
		} else {
			return latestTime;
		}
	}

	public static void main(String args[]) {
		int[] A = { 1, 3, 1, 4, 2, 3, 5, 4 };
		System.out.println(new Solution().solution(3, A));
	}

}
