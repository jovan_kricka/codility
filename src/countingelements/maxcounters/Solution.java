package countingelements.maxcounters;

import java.util.Arrays;

// https://codility.com/demo/results/trainingPQGEK9-SXY/
public class Solution {

	public int[] solution(int N, int[] A) {
		int[] counters = new int[N];
		for (int i = 0; i < counters.length; i++) {
			counters[i] = -1;
		}
		int maxCounter = 0;
		int lastFlattening = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i] >= 1 && A[i] <= N) {
				if (counters[A[i] - 1] < lastFlattening) {
					counters[A[i] - 1] = lastFlattening + 1;
				} else {
					counters[A[i] - 1]++;
				}
				if (counters[A[i] - 1] > maxCounter) {
					maxCounter = counters[A[i] - 1];
				}
			} else if (A[i] == N + 1) {
				lastFlattening = maxCounter;
			}
		}
		for (int i = 0; i < counters.length; i++) {
			if (counters[i] < lastFlattening) {
				counters[i] = lastFlattening;
			}
		}
		return counters;
	}

	public static void main(String args[]) {
		int[] A = { 3, 4, 4, 6, 1, 4, 4 };
		System.out.println(Arrays.toString(new Solution().solution(5, A)));
	}
}
