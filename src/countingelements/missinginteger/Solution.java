package countingelements.missinginteger;

import java.util.HashSet;

// https://codility.com/demo/results/training6PJEN5-2SG/
public class Solution {

	public int solution(int[] A) {
		int minMissingValue = 0;
		HashSet<Integer> uniqueElements = new HashSet<Integer>();
		for (int i = 0; i < A.length; i++) {
			uniqueElements.add(A[i]);
		}
		for (int i = 0; i < A.length; i++) {
			if (!uniqueElements.contains(i + 1)) {
				minMissingValue = i + 1;
				break;
			}
		}
		return (minMissingValue == 0 ? A.length + 1 : minMissingValue);
	}

	public static void main(String[] args) {
		int[] A = { 1 };
		System.out.println(new Solution().solution(A));

	}

}
