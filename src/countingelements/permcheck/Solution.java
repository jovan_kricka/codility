package countingelements.permcheck;

import java.util.HashSet;
import java.util.Set;

// https://codility.com/demo/results/training565KTY-6FC/
public class Solution {

	public int solution(int[] A) {
		int arrayIsAPermutation = 1;
		Set<Integer> elementsOfPermutation = new HashSet<Integer>();
		for (int i = 0; i < A.length; i++) {
			if (A[i] > 0 && A[i] <= A.length) {
				elementsOfPermutation.add(A[i]);
			}
		}
		if (elementsOfPermutation.size() < A.length) {
			arrayIsAPermutation = 0;
		}
		return arrayIsAPermutation;
	}

	public static void main(String args[]) {
		int[] A = { 4, 1, 3, 2 };
		System.out.println(new Solution().solution(A));
	}

}
