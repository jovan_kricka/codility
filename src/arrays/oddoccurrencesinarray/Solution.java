package arrays.oddoccurrencesinarray;

import java.util.HashMap;

// https://codility.com/demo/results/trainingM62VRZ-QUX/
public class Solution {

	public static int solution(int[] A) {
		HashMap<Integer, Integer> unpairedElements = new HashMap<Integer, Integer>();
		for (Integer element : A) {
			if (!unpairedElements.containsKey(element)) {
				unpairedElements.put(element, element);
			} else {
				unpairedElements.remove(element);
			}
		}
		return unpairedElements.values().toArray(new Integer[1])[0];
	}

	public static void main(String[] args) {
		int[] a1 = { 9, 3, 9, 3, 9, 7, 9 };
		System.out.println(solution(a1));
		int[] a2 = { 1, 4, 1, 5, 5, 6, 4 };
		System.out.println(solution(a2));
	}

}
