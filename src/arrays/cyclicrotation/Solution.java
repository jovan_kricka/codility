package arrays.cyclicrotation;

// https://codility.com/demo/results/trainingMFRJ7F-T4R/
public class Solution {

	public static int[] solution(int[] A, int K) {
		int[] rotatedArray = new int[A.length];
		for (int i = 0; i < A.length; i++) {
			rotatedArray[(i + K) % A.length] = A[i];
		}
		return rotatedArray;
	}

	public static void main(String[] args) {
		int[] a1 = { 3, 8, 9, 7, 6 };
		int[] solution1 = solution(a1, 1);
		for (Integer element : solution1) {
			System.out.print(" " + element);
		}
		System.out.println("");
		int[] a2 = { 3, 8, 9, 7, 6 };
		int[] solution2 = solution(a2, 3);
		for (Integer element : solution2) {
			System.out.print(" " + element);
		}
	}

}
