package timecomplexity.permmissingelem;

// https://codility.com/demo/results/trainingQ79BJ8-VFS/
public class Solution {

	public int solution(int[] A) {
		int sum = 0;
		int comparisonSum = A.length + 1;
		for (int i = 0; i < A.length; i++) {
			sum += A[i];
			comparisonSum += i + 1;
		}
		return comparisonSum - sum;
	}

	public static void main(String[] args) {
		int[] A = { 2, 3, 1, 5, 6, 7, 4, 9, 10, 12, 11 };
		System.out.println(new Solution().solution(A));
	}

}
