package timecomplexity.tapeequilibrium;

// https://codility.com/demo/results/training7DRD23-AGX/
public class Solution {

	public int solution(int[] A) {

		int minDifference = Integer.MAX_VALUE;

		int[] headingSubSets = new int[A.length - 1];
		int[] trailingSubSets = new int[A.length - 1];

		headingSubSets[0] = A[0];
		trailingSubSets[trailingSubSets.length - 1] = A[A.length - 1];

		for (int i = 1; i < A.length - 1; i++) {
			headingSubSets[i] = headingSubSets[i - 1] + A[i];
			trailingSubSets[trailingSubSets.length - 1 - i] = trailingSubSets[trailingSubSets.length - i] + A[A.length - i - 1];
		}

		int[] differences = new int[headingSubSets.length];
		for (int i = 0; i < headingSubSets.length; i++) {
			differences[i] = Math.abs(headingSubSets[i] - trailingSubSets[i]);
		}

		for(int i = 0; i < differences.length; i++) {
			if(minDifference > differences[i]) {
				minDifference = differences[i];
			}
		}
		
		return minDifference;
	}

	public static void main(String[] args) {
		int[] A = { 3, 1, 2, 4, 3 };
		System.out.println(new Solution().solution(A));
	}

}
