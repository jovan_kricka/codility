package timecomplexity.frogjmp;

// https://codility.com/demo/results/trainingDJG4S7-BQN/
public class Solution {

	public int solution(int X, int Y, int D) {		
		return ((Y - X) % D == 0 ? (Y - X) / D: ((Y - X) / D) + 1);
	}

	public static void main(String[] args) {
		System.out.println(new Solution().solution(10, 85, 10));
	}

}
