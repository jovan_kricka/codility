package prefixsums.genomicrangequery;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// https://codility.com/demo/results/training49SVZC-4YW/
public class Solution {

	private static Map<Character, Integer> IMPACT_FACTORS = new HashMap<>();

	static {
		IMPACT_FACTORS.put('A', 1);
		IMPACT_FACTORS.put('C', 2);
		IMPACT_FACTORS.put('G', 3);
		IMPACT_FACTORS.put('T', 4);
	}

	public static int[] solution(String S, int[] P, int[] Q) {
		int[] minimums = new int[P.length];
		int[] genomPrefixSum = calculateGenomPrefixSums(S);
		minimums = findSpecificMinimums(S, genomPrefixSum, minimums, P, Q, 4);
		S = modifyGenom(S, 'T', 'G');
		genomPrefixSum = calculateGenomPrefixSums(S);
		minimums = findSpecificMinimums(S, genomPrefixSum, minimums, P, Q, 3);
		S = modifyGenom(S, 'G', 'C');
		genomPrefixSum = calculateGenomPrefixSums(S);
		minimums = findSpecificMinimums(S, genomPrefixSum, minimums, P, Q, 2);
		for (int i = 0; i < minimums.length; i++) {
			if (minimums[i] == 0) {
				minimums[i] = 1;
			}
		}
		return minimums;
	}

	private static String modifyGenom(String genom, char nucleotidToBeReplaced, char nucleotidToReplaceWith) {
		StringBuilder modifiedGenomBuilder = new StringBuilder();
		for (int i = 0; i < genom.length(); i++) {
			if (genom.charAt(i) == nucleotidToBeReplaced) {
				modifiedGenomBuilder.append(nucleotidToReplaceWith);
			} else {
				modifiedGenomBuilder.append(genom.charAt(i));
			}
		}
		return modifiedGenomBuilder.toString();
	}

	private static int[] findSpecificMinimums(String s, int[] genomPrefixSum, int[] minimums, int[] p, int[] q,
			int specificMinimum) {
		for (int i = 0; i < p.length; i++) {
			if (minimums[i] == 0) {
				int numberOfNucleotidsInSequence = q[i] - p[i] + 1;
				int minimalSequenceSum = numberOfNucleotidsInSequence * specificMinimum;
				int startingPrefixSum = p[i] == 0 ? 0 : genomPrefixSum[p[i] - 1];
				int actualSequenceSum = genomPrefixSum[q[i]] - startingPrefixSum;
				if (actualSequenceSum >= minimalSequenceSum) {
					minimums[i] = specificMinimum;
				}
			}
		}
		return minimums;
	}

	private static int[] calculateGenomPrefixSums(String s) {
		int[] genomPrefixSums = new int[s.length()];
		genomPrefixSums[0] = IMPACT_FACTORS.get(s.charAt(0));
		for (int i = 1; i < s.length(); i++) {
			genomPrefixSums[i] = genomPrefixSums[i - 1] + IMPACT_FACTORS.get(s.charAt(i));
		}
		return genomPrefixSums;
	}

	public static void main(String[] args) {
		String dnaSequence = "AGCCTTAAGGC";
		int[] pQuery = { 1, 3, 7 };
		int[] qQuery = { 3, 4, 9 };
		int[] answers = solution(dnaSequence, pQuery, qQuery);
		System.out.println(Arrays.toString(answers));
	}

}
