package prefixsums.passingcars;

// https://codility.com/demo/results/trainingBJHARU-U8H/
public class Solution {

	public int solution(int[] A) {
		int pairsPassing = 0;
		int carsTravelingEast = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i] == 0) {
				carsTravelingEast++;
			} else if (A[i] == 1) {
				pairsPassing += carsTravelingEast;
			}
			if (pairsPassing > 1_000_000_000) {
				return -1;
			}
		}
		return pairsPassing;
	}

	public static void main(String[] args) {
		int[] A = { 0, 1, 0, 1, 1 };
		System.out.println(new Solution().solution(A));
	}

}
