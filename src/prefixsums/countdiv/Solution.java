package prefixsums.countdiv;

// https://codility.com/demo/results/training9STH7H-NNQ/
public class Solution {

	public int solution(int A, int B, int K) {
		int firstDivisible = (A % K == 0 ? A : A + (K - (A % K)));
		if (A == B) {
			return (A == firstDivisible ? 1 : 0);
		}
		if (firstDivisible == A) {
			return 1 + (B - A) / K;
		} else {
			return 1 + (B - firstDivisible) / K;
		}
	}

	public static void main(String[] args) {
		System.out.println(new Solution().solution(6, 11, 2));
	}

}
